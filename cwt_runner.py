#!/usr/bin/python3
"""
Run CWT rig and calculate fitness
@author: Thomas
"""
from os import listdir
from os import rename
from datetime import datetime
import os
import time
import csv
import json
import sys
from foil_api import CompactRIOInterface
from Filtering import analyzemeasurement
from lennis_module import FilterParam


class CWT():
    """
    CWT runner
    """
    def __init__(self, run_dir, sensor_dir, backup_dir, rio_ip,
                 var_list, u_inf, num_periods, correct_drag):
        self.foil_api = CompactRIOInterface(rio_ip)
        self.sensor_dir = sensor_dir
        self.backup_dir = backup_dir
        self.run_dir = run_dir
        self.individual_count = 0
        self.u_inf = u_inf
        self.num_periods = num_periods
        self.num_retries = 0
        self.failed_runs = {}
        self.report_file = os.path.join(self.run_dir, "runs.csv")
        self.correct_drag = correct_drag

        var_list = ['timestamp', 'Uinf', 'Correct_drag'] + var_list
        var_list.append('fs')
        var_list.append('highcut')
        var_list.append('lowcut')
        var_list.append('order')
        var_list.append('Foldername')
        var_list.append('Generation')
        var_list.append('Absolute_number')
        out_vars = ['efficiency', 'CT', 'Statuscode', 'St_fit', 'hoc_fit',
                    'phase_fit', 'feathering_fit', 'K_sway_fit', 'K_yaw_fit',
                    'fx drift', 'fy drift', 'mz drift']
        for var in out_vars:
            var_list.append(var)
        if not os.path.exists(self.report_file):
            self.write_row(var_list)
        self.absolute_number = self.get_absolute_number()

    def run(self, phenotype, generation, rerun=False):
        """
        Starts a run of the CWT and returns a fitness
        """
        phenotype_str = ','.join(str(x) for x in phenotype)
        if phenotype_str in self.failed_runs.keys():
            print("Fetching failed run from memory...")
            print(phenotype_str)
            return self.failed_runs[phenotype_str]
        working_dir = os.path.join(self.run_dir,
                                   str(self.individual_count))
        if not os.path.exists(working_dir):
            os.makedirs(working_dir)
        print("Starting run #{}...(Not safe to cancel)".format(
            self.individual_count))
        self.foil_api.set_movement(phenotype[0],
                                   phenotype[1],
                                   phenotype[2],
                                   phenotype[3],
                                   phenotype[4],
                                   phenotype[5],
                                   self.u_inf)
        print("Params:")
        print("k_yaw=" + str(phenotype[0]))
        print("strouhals_num=" + str(phenotype[1]))
        print("k_sway=" + str(phenotype[2]))
        print("phase_difference=" + str(phenotype[3]))
        print("feathering=" + str(phenotype[4]))
        print("sway_amp=" + str(phenotype[5]))

        period_length = 1/((phenotype[1]*self.u_inf)/(2*(phenotype[5]*0.075)))
        # If this is a rerun: Run for twice as long
        actual_periods = self.num_periods if not rerun else self.num_periods*2
        print("Run length: {}s".format(round(period_length*actual_periods)))
        time.sleep(1)  # Buffer for vars to be set (probably unnecessary)
        self.foil_api.set_output(True)
        print("Ramping up...")
        time.sleep(3+20)  # Wait for the rig to ramp up
        print("Ramped up")
        time.sleep(period_length*actual_periods)  # Run for num_periods
        self.foil_api.set_output(False)
        print("Ramping down... (Safe to cancel with ctrl+c)")
        time.sleep(3+20)  # Wait for the rig to ramp down
        print("Run complete. Analysing results...")
        time.sleep(2)   # Wait for file to be created
        sensor_data = self.read_sensors(phenotype, working_dir, generation)
        while sensor_data is None:
            time.sleep(2)
            sensor_data = self.read_sensors(phenotype, working_dir, generation)
        print(sensor_data)
        self.individual_count += 1
        print("Analysis complete")
        if sensor_data[2] == 3:
            print("Dangerously close to breaking point. Aborting...")
            self.foil_api.set_output(False)
            sys.exit(0)
        if not rerun and sensor_data[2] == 1:
            print("Precision error too high")
            print("Retrying...")
            self.num_retries += 1
            return self.run(phenotype, generation, True)
        if sensor_data[2] is not 0:
            self.failed_runs[phenotype_str] = (sensor_data[0],
                                               sensor_data[1],
                                               sensor_data[2])
        return sensor_data

    def read_sensors(self, phenotype, working_dir, generation):
        """
        Check for new sensor data
        Returns None if no new data is found
        """
        files = listdir(self.sensor_dir)
        files = [x for x in files if x.endswith('.MAT')]
        if len(files) == 0:
            return None
        elif len(files) == 1:
            result = analyzemeasurement(os.path.join(self.sensor_dir, files[0]),
                                        os.path.join(working_dir, ""),
                                        phenotype[1],
                                        phenotype[5],
                                        phenotype[3],
                                        phenotype[4],
                                        phenotype[2],
                                        phenotype[0],
                                        self.u_inf,
                                        1,
                                        self.correct_drag)
            rename(os.path.join(self.sensor_dir, files[0]),
                   os.path.join(working_dir, files[0]))

            self.write_row([str(datetime.now()),
                            self.u_inf,
                            self.correct_drag] +
                           phenotype +
                           [FilterParam.fs,
                            FilterParam.highcut,
                            FilterParam.lowcut,
                            FilterParam.order,
                            self.individual_count,
                            str(generation),
                            self.absolute_number] +
                           list(result))
            self.write_config()
            self.absolute_number += 1
            return (result[0], result[1], result[2])
        else:
            raise Exception(
                'More than 1 file in sensor dir. Likely sensor error'
                )

    def write_row(self, row):
        """
        Write a row to the report file
        """
        with open(self.report_file, "a", newline='') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=";")
            csv_writer.writerow(row)

    def write_config(self):
        """
        Write config to file
        """
        with open('config.json', 'w') as config_file:
            json.dump({'absolute_number': self.absolute_number}, config_file)

    @staticmethod
    def get_absolute_number():
        """
        Reads config file and returns absolute number
        """
        with open('config.json', 'r') as config_file:
            config = json.load(config_file)
            if 'absolute_number' in config:
                return int(config['absolute_number'])
            else:
                return 0

#!/usr/bin/python3
"""
API for interacting with foil rig controllers
@author: Thomas
"""
import urllib.request


class CompactRIOInterface():
    """
    Interface to CompactRIO
    """
    def __init__(self, ip_address):
        self.base_url = 'http://{}:8001/FoilApi/RunRig/'.format(ip_address)

    def set_movement(self,
                     k_yaw=None,
                     strouhals_num=None,
                     k_sway=None,
                     phase_difference=None,
                     feathering=None,
                     sway_amp=None,
                     u_inf=None):
        """
        Sends movement params to rig controller
        """
        url = self.base_url + "set_movement?"
        params = {}
        params["k_yaw"] = k_yaw
        params["strouhals_num"] = strouhals_num
        params["k_sway"] = k_sway
        params["phase_difference"] = phase_difference
        params["feathering"] = feathering
        params["sway_amp"] = sway_amp
        params["uinf"] = u_inf

        params = {k: v for k, v in params.items() if v is not None}
        url += '&'.join(['%s=%s' % (k, v) for (k, v) in params.items()])
        urllib.request.urlopen(url).read()

    def set_output(self, enable):
        """
        Starts or stops foil rig
        """
        url = self.base_url + "toggle_output?enable="
        url += '1' if enable else '0'
        urllib.request.urlopen(url).read()

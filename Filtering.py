# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 11:32:49 2017
@author: Lennard
"""


def analyzemeasurement(filename, workingdir, St, hoc, phase, feathering,
                       K_sway, K_yaw, Uinf, savefigure, correct_drag, doplot=True):
    """
    Calculates the efficiency based on the given raw data file (filename.MAT)
    Returns efficiency and motion fit variables.
    Returns errormessage:
        0 = no error
        1 = Retry run with increased number of oscillations
            (Precision error too high)
        2 = discard run (Motion not represented correctly)
    """
    import scipy.integrate as integrate
    import numpy as np
    import matplotlib.pyplot as plt
    import lennis_module as lm
    from tolerance_variables import tol
    from collections import namedtuple
    from lennis_module import FilterParam
    from lennis_plotfunctions import foilquiver
    import os
    #plt.ioff()  # Hides figures
    errormessage = 0
    #plt.close('all')
    """
    Parameters
    """
    # Parameters for non-dimensionalizing
    span = 0.4  # Foil span length in meters
    chord = 0.075  # Foil chord length in meters
    rho = 998.2  # Water density (change this to be tabulated by temperature)

    # Physical parameters
    foilmass = 2.34  # Mass of everything suspended to the force transducer
    foilinertia = 0.000977497997216

    # Find oscillation frequency
    freq = St * Uinf / (2 * hoc * chord)

    # Set lowcut frequency based on oscillation frequency
    FilterParam.lowcut = freq / 2

    # Parameters for seriessplitting
    Nskip = 3  # Number of periods to skip from start end end of timeseries
    derdelta = 0.01  # Margin when determining if the derivative is positive

    # zero angle of attack in the beginning
    time_zero = 20

    # Create directory for plots
    if not os.path.exists(workingdir + 'plots/'):
        os.makedirs(workingdir + 'plots/')
    # Perform necessary data processing and filtering, returns a cut datatuple
    # with the processed measurement data, cut to an number of oscillations
    (cut, periods, drift,
     newerror) = lm.process_results(filename, Nskip, derdelta, workingdir,
                                    correct_drag, savefigure, doplot)
    upcr = cut.upcr

    # Genrate foilplot with quivers
    if doplot is True:
        foilquiver(cut, periods, Uinf, workingdir, savefigure=savefigure)

    # update errormessage
    errormessage = lm.updateerrormessage(errormessage, newerror)
    del newerror

    # Determine motion parameters
    (time_array, sway_popt, swayvel, swayacc, yaw_popt, yawrate, yawacc_array
     ) = lm.determine_motion(cut, Nskip, hoc, chord, freq, derdelta,
                             workingdir, K_sway, K_yaw, phase, feathering,
                             Uinf, periods, savefigure, doplot)
    # Check if force is too high
    if max(abs(cut.fy)) > 40:
        errormessage = 3  # abort the operation

    # Split Fx, Fy and Mz. Integrate Fx to find mean thrust
    # Allocate arrays
    xsize = np.shape(time_array)[0]
    ysize = len(upcr)-1
    thrust_array = lm.createNAN(xsize, ysize)
    lift_array = lm.createNAN(xsize, ysize)
    Mzmat = lm.createNAN(xsize, ysize)
    thrust = lm.createNAN(ysize, 1)
    for i in range(0, len(upcr)-1):  # Loop through oscillations
        yawtemp = cut.yaw[upcr[i]:upcr[i+1]]
        # Decompose Fx force
        thrusttemp = (np.cos(yawtemp) * cut.fx[upcr[i]:upcr[i+1]] +
                      np.sin(yawtemp) * cut.fy[upcr[i]:upcr[i+1]])
        # Decompose Fy force
        lifttemp = (- np.sin(yawtemp) * cut.fx[upcr[i]:upcr[i+1]] +
                    np.cos(yawtemp) * cut.fy[upcr[i]:upcr[i+1]])
        # allocate to 2d-array, one column for each oscillation
        thrust_array[0:len(thrusttemp), i] = thrusttemp
        lift_array[0:len(lifttemp), i] = lifttemp
        temp = cut.mz[upcr[i]:upcr[i+1]]
        Mzmat[0:len(temp), i] = temp
        # Integrate thrust to find mean thrust
        thrust[i] = integrate.simps(np.nan_to_num(thrusttemp),
                                    dx=1/FilterParam.fs) / periods[i]
        del thrusttemp, lifttemp, temp

    if doplot:  # Plot thrust and lift through one oscillation
        plt.figure(13)
        plt.scatter(time_array, thrust_array, label='thrust', color='r', s=0.5)
        plt.scatter(time_array, lift_array, label='lift', color='g', s=0.5)
        plt.legend(loc="best")
        plt.grid(True)

    # Check if the measured mean thrust is within a set uncertainty limit
    thrust_uncertainty, thrust_mean = lm.precerror(thrust)

    # Thrust coefficient
    CT = - thrust_mean / (0.5 * rho * Uinf**2 * chord * span)
    """
    print('CT = %f' % CT)
    """
    # Sway contribution with inertia contribution subtracted
    powerinput1 = np.multiply(swayvel, -lift_array)
    swayinertiapower = np.multiply(swayvel, foilmass * swayacc)

    # Yaw contribution
    powerinput2 = np.multiply(yawrate, -Mzmat)
    yawinertiapower = np.multiply(yawrate, foilinertia * yawacc_array)
    powerinput = powerinput1 - swayinertiapower + powerinput2 - yawinertiapower

    if doplot:  # Plot power distribution
        plt.figure(12, figsize=(12, 12))
        plt.subplot(2, 1, 1)
        plt.scatter(time_array, swayinertiapower, label="inertial",
                    color='r', s=0.5)
        plt.scatter(time_array, powerinput1, label="raw lift power",
                    color='g', s=0.5)
        plt.scatter(time_array, powerinput1 + swayinertiapower,
                    label="without inertia", color='b', s=0.5)
        plt.legend(loc="best")
        plt.ylabel('Power [N*m/s]')
        plt.title('Sway')
        plt.grid(True)

        plt.subplot(2, 1, 2)
        plt.scatter(time_array, powerinput2, label="raw torque",
                    color='g', s=0.5)
        plt.scatter(time_array, yawinertiapower, label="inertia",
                    color='r', s=0.5)
        plt.scatter(time_array, powerinput2 + yawinertiapower,
                    label="without inertia", color='b', s=0.5)
        plt.ylabel('Power [N*m/s]')
        plt.legend(loc="best")
        plt.title('Yaw')
        plt.grid(True)
        if savefigure == 1:
            plt.savefig(workingdir + 'plots/' + 'Inertiasubtraction.pdf')

    # calculate efficiency, and integrate to find mean efficiency
    power = lm.createNAN(powerinput.shape[1], 1)
    for i in range(0, len(power)):
        # mean power consumption in one flap
        power[i] = integrate.simps(np.nan_to_num(abs(powerinput[:, i])),
                                   dx=1/FilterParam.fs) / (periods[i])
    # mean power consumption between all flaps
    power_uncertainty, power_mean = lm.precerror(power)

    # Power coefficient
    CP = power_mean / (0.5 * rho * Uinf**3 * chord * span)
    CP_error = power_uncertainty / (0.5 * rho * Uinf**3 * chord * span)
    efficiency = CT / CP
    # print('efficiency = %2.2f %%' % (efficiency*100))

    # Calculate angle of attack in one flap
    yawtemp = cut.yaw[upcr[0]:upcr[1]] * 180 / np.pi
    swaytemp = swayvel[:, 0]
    swaytemp = swaytemp[~np.isnan(swaytemp)]  # Delete NaN entries
    AoA = -np.arctan( swaytemp / Uinf) * 180 / np.pi + yawtemp
    max_AoA = max(AoA)
    min_AoA = min(AoA)
    timetemp = time_array[:, 0]
    timetemp = timetemp[~np.isnan(timetemp)]  # Delete NaN entries
    if doplot:  # Plot angle of attack, incident velocity and drag - lift forces
        plt.figure(5)
        ax0 = plt.subplot(3, 1, 1)
        plt.grid(True)
        plt.plot(timetemp, yawtemp, label="yaw")
        plt.plot(timetemp, AoA, label="AoA")
        plt.legend(loc="best")
        plt.ylabel('Angle [deg]')
        ax1 = plt.subplot(3, 1, 2, sharex=ax0)
        plt.grid(True)
        plt.scatter(time_array, lift_array, label="Flift", s=0.5, color='r')
        plt.scatter(time_array, thrust_array, label="Fdrag", s=0.5, color='g')
        plt.ylabel('Force [N]')
        plt.legend(loc="best")
        # Plot incident velocity
        plt.subplot(3, 1, 3, sharex=ax0)
        plt.grid(True)
        incidentvel = (Uinf * np.cos(AoA*np.pi/180) -
                       swaytemp * np.sin(AoA*np.pi/180))
        plt.plot(timetemp, incidentvel)
        plt.ylabel('Incident Velocity [m/s]')
        ax1.twinx()
        plt.xlabel('time relative to period [t/T]')
    # Return the fitted motion variables
    St_fit = 2 * sway_popt[1] * hoc * chord / Uinf
    K_sway_fit = sway_popt[3]
    K_yaw_fit = yaw_popt[3]
    phase_fit = yaw_popt[2]
    hoc_fit = sway_popt[0] / chord
    feathering_fit = yaw_popt[0] / np.arctan(sway_popt[0] * 2 * np.pi *
                                             sway_popt[1] / Uinf)
    # Check if the motion is represented correctly
    motionerrors = namedtuple('motionerrors',
                              'strouhal K_sway K_yaw feathering phase swayamp')
    deviation = motionerrors(strouhal=abs(St - St_fit),
                             K_sway=abs(K_sway - K_sway_fit),
                             K_yaw=abs(K_yaw - K_yaw_fit),
                             feathering=abs(feathering - feathering_fit),
                             phase=abs(phase - phase_fit),
                             swayamp=abs(hoc - hoc_fit))
    """
    print('Error in motion representation:')
    for fld in motionerrors._fields:
        print(fld + ' = ' + ('%2.4f' % getattr(deviation, fld)))
    """
    if power_uncertainty > tol.power:
        print('Measured power consumption uncertainty is above set'
              ' limit, measured at %2.1f %%' % (power_uncertainty*100))
        errormessage = lm.updateerrormessage(errormessage, 1)
    # Check if the efficiency is physical
    if efficiency > lm.idealefficiency(CT):
        print('Measured efficiency above ideal efficiency. '
              'Measured efficiency = %2.1f %%, Ideal efficiency =  %2.1f %%'
              % ((efficiency * 100), (lm.idealefficiency(CT) * 100)))
        errormessage = lm.updateerrormessage(errormessage, 1)
    if thrust_uncertainty > tol.thrust:
        if CT > 0.01:
            print('Measured mean thrust uncertainty is above set limit,'
                  ' measured at %2.1f %%' % (thrust_uncertainty * 100))
            errormessage = lm.updateerrormessage(errormessage, 1)
        else:
            errormessage = lm.updateerrormessage(errormessage, 2)
    if deviation.strouhal > tol.strouhal:
        print('Strouhals number outside motion tolerance, '
              'deviation measured at %2.4f' % (deviation.strouhal))
        errormessage = lm.updateerrormessage(errormessage, 2)
    if deviation.K_sway > tol.K_sway:
        print('K_sway outside motion tolerance, '
              'deviation measured at %2.4f' % (deviation.K_sway))
        errormessage = lm.updateerrormessage(errormessage, 2)
    if deviation.K_yaw > tol.K_yaw:
        print('K_yaw outside motion tolerance, '
              'deviation measured at %2.4f' % (deviation.K_yaw))
        errormessage = lm.updateerrormessage(errormessage, 2)
    if deviation.feathering > tol.feathering:
        print('Feathering parameter outside motion tolerance, '
              'deviation measured at %2.4f' % (deviation.feathering))
        errormessage = lm.updateerrormessage(errormessage, 2)
    if deviation.phase > tol.phase:
        print('Phase outside motion tolerance, '
              'deviation measured at %2.4f' % (deviation.phase))
        errormessage = lm.updateerrormessage(errormessage, 2)
    if deviation.swayamp > tol.swayamp:
        print('Sway amplitude outside motion tolerance, '
              'deviation measured at %2.4f' % (deviation.swayamp))
        errormessage = lm.updateerrormessage(errormessage, 2)
    """
    print('thrust uncertainty: %f' % thrust_uncertainty)
    print('power uncertainty: %f' % power_uncertainty)
    """
    return (efficiency.item(), CT, errormessage,
            St_fit, hoc_fit, phase_fit, feathering_fit, K_sway_fit,
            K_yaw_fit, drift[0], drift[1], drift[2], max_AoA, min_AoA)

"""
Simple graphics module for GA
@author: Thomas
"""
import turtle
import colorsys


class GAGraphics():
    """
    Graphic functions
    """
    def __init__(self, queue):
        self.queue = queue
        self.width = 1100
        self.height = 950
        self.circle_size = 70
        self.queue.put(lambda: self.__setup())

    def __setup(self):
        self.screen = turtle.Screen()
        self.screen.setup(width=self.width, height=self.height,
                          startx=-1, starty=0)
        turtle.setundobuffer(60)
        self.reset()

    def draw_population(self, calculated_population):
        self.queue.put(lambda: self.__draw_population(calculated_population))

    def __draw_population(self, calculated_population):
        self.draw_heading("Population", 20)
        self.move_y(1.2)
        for individual in calculated_population:
            self.draw_next_circle(self.scaled_color(individual[0]),
                                  self.make_individual_text(individual[2],
                                                            individual[3],
                                                            individual[0]))
        turtle.color("black")
        self.move_leftmost()
        self.move_y(0.5)
        self.draw_heading("Best individual", 20)
        self.move_y(1.2)
        if len(calculated_population) > 0:
            best = max(calculated_population)
            self.draw_next_circle(self.scaled_color(best[0]),
                                  self.make_individual_text(best[2],
                                                            best[3],
                                                            best[0]))

    def set_current_run(self, text, rerun):
        self.queue.put(lambda: self.__set_current_run(text, rerun))

    def __set_current_run(self, text, rerun):
        self.move_y(-1.0)
        self.move_x(-1.5)
        self.draw_heading("Current run" + (" (random)" if rerun else ""), 20)
        self.move_y(1)
        self.draw_heading(text, 14)
        self.screen.update()

    def set_generation(self, num):
        self.queue.put(lambda: self.__set_generation(num))

    def __set_generation(self, num):
        self.reset()
        self.draw_heading("Generation #{}".format(num), 24)
        self.move_y(0.4)

    def set_info(self, text):
        self.queue.put(lambda: self.__set_info(text))

    def __set_info(self, text):
        self.move_y(-1.2)
        self.move_x(-1.5)
        self.draw_heading("Info", 20)
        self.move_y(1)
        self.draw_heading(text, 14)


    # Private

    def draw_next_circle(self, color, text):
        turtle.pendown()
        turtle.begin_fill()
        turtle.color(color)
        turtle.circle(self.circle_size)
        turtle.end_fill()
        turtle.color("black")
        self.move_y(-0.1)
        self.move_x(0.3)
        turtle.write(text, align="left", font=("Arial", 9, "normal"))
        self.move_x(-0.3)
        self.move_y(0.1)
        turtle.penup()
        if self.circle_size*3 + turtle.position()[0] >= self.screen.window_width() / 2:
            turtle.setposition(self.leftmost(),
                               turtle.position()[1]-self.circle_size*2)
        else:
            turtle.forward(self.circle_size*2)

    def draw_heading(self, text, size):
        self.move_x(0.4)
        turtle.write(text,align="left", font=("Arial", size, "normal"))
        self.move_x(-0.4)

    def reset(self):
        self.screen.clear()
        self.screen.tracer(0, 0)
        turtle.color("black")
        turtle.hideturtle()
        turtle.speed(0)
        turtle.penup()
        turtle.setposition(self.leftmost(),
                           self.topmost())

    @staticmethod
    def scaled_color(value):
        return colorsys.hsv_to_rgb(value/3, 1.0, 1.0)

    def move_y(self, factor):
        turtle.penup()
        turtle.setposition(turtle.position()[0],
                    turtle.position()[1] - self.circle_size*(2*factor))
        turtle.pendown()

    def move_x(self, factor):
        turtle.penup()
        turtle.setposition(turtle.position()[0] - self.circle_size*(2*factor),
                           turtle.position()[1])

    def make_individual_text(self, phenotype, thrust, efficiency):
        from optimize_movement import MovementOptimization
        p_list = ["Efficiency: {}%".format(int(efficiency*100))]
        p_list += MovementOptimization.phenotype_human_readable(phenotype)
        p_list.append("thrust: " + "%.2f" % thrust)
        return "\n".join(p_list)

    def leftmost(self):
        return (-self.screen.window_width()/2)+self.circle_size + 2

    def topmost(self):
        return (self.screen.window_height()/2)-self.circle_size

    def move_leftmost(self):
        turtle.penup()
        turtle.setx(self.leftmost())

"""
GA GUI
@author: Thomas
"""
import tkinter
import queue


def init_gui(callback_queue, ga):
    """
    Build GUI and run mainloop
    """
    top = tkinter.Tk()

    def check_queue():
        """
        Check gui action queue
        """
        try:
            callback = callback_queue.get(False)
        except queue.Empty:
            pass
        else:
            callback()
        top.after(1, check_queue)

    top.geometry("250x430+0+0")

    def stop_run():
        """
        Send stop signal to api and quit
        """
        try:
            print("Sending stop signal")
            ga.problem.cwt.foil_api.set_output(False)
            print("Stop signal sent")
        except Exception:
            print("Unable to send stop signal to API")
        top.quit()

    quit_button = tkinter.Button(top,
                                 text="Stop",
                                 command=stop_run,
                                 height=5,
                                 width=10,
                                 bg="red",
                                 activebackground="darkred")
    quit_button.pack()

    m_var = tkinter.IntVar()
    m_var.set(ga.problem.mutation_rate*100)
    ms_var = tkinter.IntVar()
    ms_var.set(ga.problem.mutation_strength*100)
    c_var = tkinter.IntVar()
    c_var.set(ga.problem.crossover_rate*100)
    t_var = tkinter.IntVar()
    t_var.set(ga.problem.tournament_size)

    def update_vars():
        """
        Set GA variables from GUI
        """
        ga.problem.mutation_rate = m_var.get()/100.0
        ga.problem.crossover_rate = c_var.get()/100.0
        ga.problem.tournament_size = t_var.get()
        ga.problem.mutation_strength = ms_var.get()/100.0
        label.config(
            text="Mutation={}\n"
                 "Mutation strength={}\n"
                 "Crossover={}\n"
                 "Tournament={}".format(
                     ga.problem.mutation_rate,
                     ga.problem.mutation_strength,
                     ga.problem.crossover_rate,
                     ga.problem.tournament_size))

    cross_scale = tkinter.Scale(top,
                                variable=c_var,
                                orient=tkinter.HORIZONTAL,
                                label="Crossover rate [%]",
                                length=200)
    cross_scale.pack(anchor=tkinter.CENTER)

    mut_scale = tkinter.Scale(top,
                              variable=m_var,
                              orient=tkinter.HORIZONTAL,
                              label="Mutation rate [%]",
                              length=200)
    mut_scale.pack(anchor=tkinter.CENTER)

    mut_s_scale = tkinter.Scale(top,
                                variable=ms_var,
                                orient=tkinter.HORIZONTAL,
                                label="Mutation strength [%]",
                                length=200)
    mut_s_scale.pack(anchor=tkinter.CENTER)

    tournament_scale = tkinter.Scale(top,
                                     variable=t_var,
                                     orient=tkinter.HORIZONTAL,
                                     label="Tournament size",
                                     length=200)
    tournament_scale.pack(anchor=tkinter.CENTER)

    button = tkinter.Button(top,
                            text="Update",
                            command=update_vars,
                            bg="lightblue",
                            activebackground="cyan")
    button.pack(anchor=tkinter.CENTER)

    label = tkinter.Label(top)
    label.config(text="Mutation=\nMutation strength=\nCrossover=\nTournament size=")
    label.pack()

    def decimate():
        """
        Tell GA to decimate next generation
        """
        ga.decimate = True

    decimate_button = tkinter.Button(top,
                                     text="Decimate population",
                                     command=decimate,
                                     bg="red",
                                     activebackground="darkred")
    decimate_button.pack(anchor=tkinter.CENTER)

    top.after(1, check_queue)
    top.mainloop()

# -*- coding: utf-8 -*-
"""
Created on Sun Feb 26 14:07:45 2017
Functions used in the master thesis on oscillating foil optimization
@author: Lennard
"""
import scipy.io
from scipy.signal import butter, filtfilt, freqz
from scipy.signal import sosfreqz, sosfilt, sosfiltfilt
import matplotlib.pyplot as plt
import numpy as np
import os
from tolerance_variables import tol
from collections import namedtuple

class FilterParam:
    """
    Filtering parameters which are set in the script Filtering.py
    """
    fs = 200  # Sample rate
    highcut = 6  # high cutoff frequency
    lowcut = 0  # low cutoff frequency
    order = 16  # Filter order


def butter_bandpass(lowcut, highcut, fs, order=3):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='bandpass')
    return b, a


def butter_lowpass(highcut, fs, order=3):
    nyq = 0.5 * fs
    high = highcut / nyq
    b, a = butter(order, high, btype='lowpass')
    return b, a


def butter_lowpass_filter(data, highcut, fs, order=3):
    b, a = butter_lowpass(highcut, fs, order=order)
    y = filtfilt(b, a, data)
    return y


def butter_bandpass_filter(data, lowcut, highcut, fs, order=3):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = filtfilt(b, a, data)
    return y


def buttersos(lowcut, highcut, fs, order=12):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    sos = butter(order, [low, high], btype='bandpass', output='sos')
    return sos


def buttersos_lowpass(highcut, fs, order=12):
    nyq = 0.5 * fs
    high = highcut / nyq
    sos = butter(order, high, btype='lowpass', output='sos')
    return sos


def soslowpass(signal, highcut, fs, order=12):
    sos = buttersos_lowpass(highcut, fs, order)
    return sosfiltfilt(sos, signal)


def sosbandpass(signal, lowcut, highcut, fs, order=8, zero_phase=True):
    """Filter signal with band-pass filter.

    :param signal: Signal
    :param lowcut: Lower cut-off frequency
    :param highcut: Upper cut-off frequency
    :param fs: Sample frequency
    :param order: Filter order
    :param zero_phase: Prevent phase error by filtering in both directions (filtfilt)

    A Butterworth filter is used. Filtering is done with second-order sections.

    .. seealso:: :func:`bandpass_filter` for the filter that is used.

    """
    sos = buttersos(lowcut, highcut, fs, order)
    if zero_phase:
        return sosfiltfilt(sos, signal)
    else:
        return sosfilt(sos, signal)


def precerror(series):
    """
    Calculates the uncertainty of a array of measured results
    """
    from scipy.stats import t
    # Calculate statistical parameters
    df = np.size(series)  # Number of degrees of freedom
    t_student = t.interval(0.95, df, loc=0, scale=1)
    std_series = np.std(series)
    mean_series = np.mean(series)
    # Find precision limit of the mean result
    Px_mean = std_series / np.sqrt(df) * t_student[1]
    uncertainty = np.abs(Px_mean / mean_series)
    return uncertainty, mean_series


def readchannel(matfile, channelnumber, channelname):
    """
    Reads the data for the selected channel from the matfile, and checks
    if it corresponds to the channelname
    """
    # Check if the channelname corresponds to the channelnumber
    temp = matfile['Channel_%d_Header' % channelnumber]
    if np.array_str(temp[0][0][3]) != channelname:
        raise Exception('Name of the channel %d is wrong' % channelnumber)
    # Read the channel data
    output = np.squeeze(matfile['Channel_%d_Data' % channelnumber])
    return output


def Kprofilefit(time, data, amp, f, phase, K, plotlabel, workingdir, issway,
                doplot, savefigure):
    """
    Fits a curve based on Lu et.al. nonsinusoidal oscillation pattern to a
    measured timeseries
    """
    from scipy.optimize import curve_fit
    if doplot:  # Create subplot if the sway has already been plotted
        if plt.fignum_exists(6) is True:
            plt.figure(6)
            plt.subplot(2, 1, 2)
        else:
            plt.figure(6)
            plt.subplot(2, 1, 1)
        plotrng = int(200 / f)
        plt.scatter(time[0:plotrng], data[0:plotrng], color='red', label='raw')
        plt.title('Sway and yaw function fit')
    if abs(K) < 0.01:
        if issway is 0:
            def func1(t, amp, f, phase):
                return amp*np.sin(2 * np.pi * f * t + phase)
            popt, pcov = curve_fit(func1, time, data, p0=[amp, f, phase],
                                   bounds=([0, 0.05, -0.1], [1.3, 2., 6]),
                                   maxfev=4000)
            if doplot:  # plot figures
                plt.plot(time[0:plotrng], func1(time[0:plotrng], *popt),
                         label=plotlabel)
                plt.plot(time[0:plotrng], func1(time[0:plotrng], amp, f, phase),
                         label="input")
        elif issway is 1:
            def func1(t, amp, f):
                return amp*np.sin(2 * np.pi * f * t)
            popt, pcov = curve_fit(func1, time, data, p0=[amp, f],
                                   bounds=([0.015, 0.05], [0.113, 2.]),
                                   maxfev=4000)
            if doplot:  # plot figures
                plt.plot(time[0:plotrng], func1(time[0:plotrng], *popt),
                         label=plotlabel)
                plt.plot(time[0:plotrng], func1(time[0:plotrng], amp, f),
                         label="input")
            popt = np.append(popt, 0)  # set phase to 0
        # append K value
        popt = np.append(popt, K)
    elif K > 0:
        if issway is 0:
            def func2(t, amp, f, phase, K):
                return amp * (
                    np.tanh(K * np.sin(2 * np.pi * f * t + phase)) /
                    np.tanh(K))
            popt, pcov = curve_fit(func2, time, data, p0=[amp, f, phase, K],
                                   bounds=([0, 0.05, -0.1, 0.01],
                                           [1.3, 2., 6, 3]),
                                   maxfev=4000)
            if doplot:  # plot figures
                plt.plot(time[0:plotrng], func2(time[0:plotrng], *popt),
                         label=plotlabel)
                plt.plot(time[0:plotrng], func2(time[0:plotrng], amp, f, phase, K),
                         label="input")
        elif issway is 1:
            def func2(t, amp, f, K):
                return amp * (
                    np.tanh(K * np.sin(2 * np.pi * f * t)) /
                    np.tanh(K))
            popt, pcov = curve_fit(func2, time, data, p0=[amp, f, K],
                                   bounds=([0.015, 0.05, 0.01], [0.113, 2., 3]),
                                   maxfev=4000)
            if doplot:  # plot figures, plot original function
                plt.plot(time[0:plotrng], func2(time[0:plotrng], amp, f, K),
                         label="input")
                plt.plot(time[0:plotrng], func2(time[0:plotrng], *popt),
                         label=plotlabel)
            popt = np.append(popt, popt[2])
            popt[2] = 0  # set phase to 0
    elif K < 0:
        if issway is 0:
            def func3(t, amp, f, phase, K):
                return amp * (
                    np.arcsin(-K * np.sin(2 * np.pi * f * t + phase)) /
                    np.arcsin(-K))
            popt, pcov = curve_fit(func3, time, data, p0=[amp, f, phase, K],
                                   bounds=([0, 0.05, -0.1, -1],
                                           [1.3, 2, 6, -0.01]),
                                   maxfev=4000)
            if doplot:  # plot figures
                plt.plot(time[0:plotrng], func3(time[0:plotrng], *popt),
                         label=plotlabel)
                plt.plot(time[0:plotrng], func3(time[0:plotrng], amp, f, phase, K),
                         label="input")
        elif issway is 1:
            def func3(t, amp, f, K):
                return amp * (
                    np.arcsin(-K * np.sin(2 * np.pi * f * t)) /
                    np.arcsin(-K))
            popt, pcov = curve_fit(func3, time, data, p0=[amp, f, K],
                                   bounds=([0.015, 0.05, -1],
                                           [0.113, 2, -0.01]),
                                   maxfev=4000)
            if doplot:  # plot figures
                plt.plot(time[0:plotrng], func3(time[0:plotrng], *popt),
                         label=plotlabel)
                plt.plot(time[0:plotrng], func3(time[0:plotrng], amp, f, K),
                         label="input")
            popt = np.append(popt, popt[2])
            popt[2] = 0  # set phase to 0
    if doplot:  # plot figures
        plt.legend(loc='best')
        if issway is 0 and savefigure is True:
            plt.savefig(workingdir + 'plots/' + 'functionfit.pdf')
    return popt


def Kprofile(time, amp, f, phase, K):
    """
    Generates a nonsinusoidal oscillation pattern based on Lu et.all paper
    """
    if K == 0:
        output = amp * np.sin(2 * np.pi * f * time + phase)
    elif K > 0:
        output = amp * (
            np.tanh(K * np.sin(2 * np.pi * f * time + phase)) /
            np.tanh(K))
    elif K < 0:
        output = amp * (
                np.arcsin(-K * np.sin(2 * np.pi * f * time + phase)) /
                np.arcsin(-K))
    return output


def Kprofilevelocity(time, amp, f, phase, K):
    """
    Generates a nonsinusoidal oscillation velocity based on Lu et.all paper
    Input time as a array!
    """
    w = 2 * np.pi * f
    output = createNAN(np.shape(time), 1)

    if K == 0:
        output = amp * w * np.cos(w * time + phase)
    elif K < 0:
        output = amp * K * w * np.cos(w * time + phase) / (
            np.arcsin(K) * np.sqrt(1 - K ** 2 * np.sin(w * time + phase) ** 2))
    elif K > 0:
        # This has to be solved numerical
        pos = Kprofile(time, amp, f, phase, K)
        # Differentiate the input matrix
        output[:-1] = np.diff(pos) * 200
        np.append(output, (pos[0] - pos[-1]) * 200)
    return output


def Kprofileacceleration(time, amp, f, phase, K):
    """
    Generates a nonsinusoidal oscillation acceleration based on Lu et.all paper
    Input time as a array!
    """

    output = createNAN(np.shape(time), 1)
    pos = Kprofile(time, amp, f, phase, K)
    # Differentiate every column of the input matrix
    output[:-1] = np.diff(pos) * 200
    np.append(output, (pos[0] - pos[-1]) * 200)
    output[:-1] = np.diff(output) * 200
    np.append(output, (output[0] - output[-1]) * 200)
    return output


def Kprofile_maxacceleration(strouhal, feathering, hoc, uinf, phase, K):
    """
    Calculates the maximum acceleration during one oscillation for the K-profile
    motion, returns in units deg/s^2
    """
    dt = 0.002
    chord = 0.075
    # calculate frequency and amplitude from dimensionless variables
    f = strouhal * uinf / (2 * hoc * chord)
    amp = feathering * np.arctan(hoc * chord * 2 * np.pi * f / uinf) * 180 / np.pi
    # Numeric differentiation
    time = np.arange(0, 1/f, dt)
    pos = Kprofile(time, amp, f, phase, K)
    vel = np.diff(pos) / dt
    vel = np.append(vel, (pos[0] - pos[-1]) / dt)
    acc = np.diff(vel) / dt
    acc = np.append(acc, (vel[0] - vel[-1]) / dt)
    accmax = max(abs(acc))
    return accmax


def maxAoA(sway_amp, st, feathering, phase, K_sway, K_yaw, u_inf,
           chord=0.075):
    # Oscillation frequency
    freq = st * u_inf / (2 * sway_amp * chord)
    time = np.arange(0, 1, 41) / freq
    # Find sway velocity for one period
    AoA = np.empty(len(time))
    i = 0
    for t in time:
        swayvel = Kprofilevelocity(t, sway_amp, freq, phase, K_sway)
        yaw = Kprofile(t, yaw_amp, f, phase, K_yaw)
        beta = np.atan(swayvel/u_inf)
        i+=1




def idealefficiency(CT):
    """
    Calculate the ideal efficiency based on given thrust-coefficient
    """
    efficiency = 2 / (1 + np.sqrt(1 + CT))
    return efficiency


def createNAN(x, y):
    """
    Creates a matrix consisting with NAN with size input x*y
    """
    if y == 1:
        output = np.empty(x)
    else:
        output = np.empty([x, y])
    output[:] = np.NAN
    return output


def readrawdata(filename):
    """
    Read raw data from the .mat result file and filter it with a butterworth
    bandpass filter
    """
    datatuple = namedtuple('datatuple', 'time sway yaw mz acc1 acc2 fx fy upcr')
    matfile = scipy.io.loadmat(filename)
    time = readchannel(matfile, 1, "['Time__1_-_default_sample_rate']")
    sway = readchannel(matfile, 6, "['PosY']") / 1000
    yaw = readchannel(matfile, 7, "['Angle']") * np.pi / 180  # convert to rads
    mz = readchannel(matfile, 19, "['Mz']")
    acc1 = - readchannel(matfile, 4, "['Acc-20496']")  # sway acc
    acc2 = readchannel(matfile, 5, "['Acc-20495']")
    fx = readchannel(matfile, 15, "['Fx']")
    fy = readchannel(matfile, 16, "['Fy']")
    # Combine everything in a tuple
    raw = datatuple(time, sway, yaw, mz, acc1, acc2, fx, fy, 0)
    return (raw, datatuple)


def filterdata(raw, correct_drag, workingdir, doplot, savefigure=0, time_zero=20, latexplot=False):
    """
    Performs lowpassfiltering on the inputted raw data, and does a drift
    correction
    """
    import sys
    sys.path.append(r"C:\Users\Lennard\Documents\GitHub\FoilOptimization\python\plotfunctions")
    from LaTeX_plot import latexsave, newfig, doubleticks

    datatuple = namedtuple('datatuple', 'time sway yaw mz acc1 acc2 fx fy upcr')
    if doplot:  # Plot filter frequency response for different orders
        plt.figure(1)
        plt.clf()
        plt.subplot(2, 1, 1)
        for order in [4, 6, 8, FilterParam.order]:
            sos = buttersos_lowpass(FilterParam.highcut, FilterParam.fs,
                                    order)
            w, h = sosfreqz(sos)
            plt.plot((FilterParam.fs * 0.5 / np.pi) * w, abs(h),
                     label="order = %d" % order)
        plt.plot([0, 0.5 * FilterParam.fs], [np.sqrt(0.5), np.sqrt(0.5)],
                 '--', label='sqrt(0.5)')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Gain')
        plt.grid(True)
        plt.legend(loc='best')
        plt.xlim([0, 20])
        plt.subplot(2, 1, 2)
        plt.title('Bandpass')
        for order in [4, 6, 8, FilterParam.order]:
            sos = buttersos(FilterParam.lowcut, FilterParam.highcut, FilterParam.fs,
                                    order)
            w, h = sosfreqz(sos)
            plt.plot((FilterParam.fs * 0.5 / np.pi) * w, abs(h),
                     label="order = %d" % order)
        plt.plot([0, 0.5 * FilterParam.fs], [np.sqrt(0.5), np.sqrt(0.5)],
                 '--', label='sqrt(0.5)')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Gain')
        plt.xlim([0, 20])
        plt.grid(True)
        plt.legend(loc='best')

    if FilterParam.lowcut is 0:  # perform SOS lowpass filtering
        yaw = soslowpass(raw.yaw, FilterParam.highcut,
                         FilterParam.fs, order=FilterParam.order)
        sway = soslowpass(raw.sway, FilterParam.highcut,
                           FilterParam.fs, order=FilterParam.order)
        acc1 = soslowpass(raw.acc1, FilterParam.highcut,
                           FilterParam.fs, order=FilterParam.order)
        acc2 = soslowpass(raw.acc2, FilterParam.highcut,
                           FilterParam.fs, order=FilterParam.order)
    else:    # Perform SOS bandpass filtering

        yaw = sosbandpass(raw.yaw, FilterParam.lowcut, FilterParam.highcut,
                          FilterParam.fs, order=FilterParam.order)
        sway = sosbandpass(raw.sway, FilterParam.lowcut, FilterParam.highcut,
                           FilterParam.fs, order=FilterParam.order)
        acc1 = sosbandpass(raw.acc1, FilterParam.lowcut, FilterParam.highcut,
                           FilterParam.fs, order=FilterParam.order)
        acc2 = sosbandpass(raw.acc2, FilterParam.lowcut, FilterParam.highcut,
                           FilterParam.fs, order=FilterParam.order)

    # Perform SOS lowpass filtering
    fx = soslowpass(raw.fx, FilterParam.highcut,
                    FilterParam.fs, order=FilterParam.order)
    fy = soslowpass(raw.fy, FilterParam.highcut,
                    FilterParam.fs, order=FilterParam.order)
    mz = soslowpass(raw.mz, FilterParam.highcut,
                    FilterParam.fs, order=FilterParam.order)
    # Measure mean forces at steady angle of attack to correct for sensor drift
    t_steady = int(time_zero*FilterParam.fs)  # time with steady angle of attack
    t_sub = int(1*FilterParam.fs)  # time to subtract in the beginning and end of steady AoA
    fx_initial = np.mean(fx[t_sub:(t_steady-t_sub)])
    fx_final = np.mean(fx[-(t_steady-t_sub):-t_sub])
    fy_initial = np.mean(fy[t_sub:(t_steady-t_sub)])
    fy_final = np.mean(fy[-(t_steady-t_sub):-t_sub])
    mz_initial = np.mean(mz[t_sub:(t_steady-t_sub)])
    mz_final = np.mean(mz[-(t_steady-t_sub):-t_sub])
    fx_corrected = driftcorrection('Fx', fx, fx_initial, fx_final, t_steady,
                                   correct=correct_drag)
    fy_corrected = driftcorrection('Fy', fy, fy_initial, fy_final, t_steady,
                                   correct=0)
    mz_corrected = driftcorrection('Mz', mz, mz_initial, mz_final, t_steady,
                                   correct=0)
    # Save sensor drift to be returned
    drift = [fx_initial-correct_drag, fy_initial, mz_initial]
    if latexplot:
        fig1, ax1 = newfig(4522, 0.8)
        ax1.plot(raw.time, raw.fx, lw=0.2, label='Original')
        ax1.plot(raw.time, fx_corrected, linewidth=1, label='Drift-corrected')
        ax1.plot(raw.time, fx, ls='--', linewidth=0.5, label='Filtered', c='r')
        ax1.set_xlabel(r'time [s]')
        ax1.set_ylabel(r'Force [N]')
        ax1.legend(loc="best")
        fig1.tight_layout()
    if doplot:  # timeseries of the filtered and unfiltered Fx, Fy force
        plt.figure(2)
        plt.subplot(3, 1, 1)
        plt.plot(raw.time, raw.fy, label='Original')
        plt.plot(raw.time, fy_corrected, linewidth=4, label='driftcorrected')
        plt.plot(raw.time, fy, ls='--', linewidth=3, label='Filteret')
        plt.title('Fy')
        plt.subplot(3, 1, 2)
        plt.plot(raw.time, raw.fx, label='Original signal')
        plt.plot(raw.time, fx_corrected, linewidth=4, label='driftcorrected')
        plt.plot(raw.time, fx, ls='--', linewidth=3, label='Filteret signal')
        plt.title('Fx')
        plt.subplot(3, 1, 3)
        plt.plot(raw.time, raw.mz, label='Original signal')
        plt.plot(raw.time, mz_corrected, linewidth=4, label='driftcorrected')
        plt.plot(raw.time, mz, ls='--', linewidth=3, label='Filteret signal')
        plt.title('Mz')
        for i in [1, 2, 3]:
            plt.subplot(3, 1, i)
            plt.grid(True)
            plt.axis('tight')
            plt.legend(loc='best')
            plt.xlabel('time [s]')
            plt.ylabel('Force [N]')
        if savefigure is 1:
            plt.savefig(os.path.join(workingdir, 'plots', 'Filtering.pdf'))

    # return as named tuple
    filt = datatuple(time=raw.time,
                     sway=sway,
                     yaw=yaw,
                     mz=mz,
                     acc1=acc1,
                     acc2=acc2,
                     fx=fx_corrected,
                     fy=fy_corrected,
                     upcr=0)
    return filt, drift


def driftcorrection(name, signal, initial, final, t_steady, correct=0):
    """
    Performs a linear interpolation correction if the signal has driftet from
    the correct value based on the inital and final measurement
    """
    # Check how much the signal has driftet
    drift = (initial-final)  # Fx sensordrift
    if abs(drift) > 0.01 and name == 'mz':
        print(name + ' sensor drift during measurement is more than %1.2f Nm'
              % drift)
    elif abs(drift) > 0.03:
        print(name + ' sensor drift during measurement is more than %1.2f N'
              % drift)
    # span between initial and final measurement
    span = len(signal) - t_steady
    incline = (initial - final)/span
    # Perform drift correction
    output = np.zeros(len(signal))
    for i in range(0, len(signal), 1):
        if i > int(t_steady) and i < int(span+t_steady)+1:
            # add interpolated component
            output[i] = signal[i] + incline * abs(i-t_steady)
        else:
            output[i] = signal[i]
        # add correct mean value
        output[i] = output[i] - initial + correct
        if abs(i-t_steady) > span:
            print('i = %f' % (i-t_steady))
    return output


def determine_motion(filt, Nskip,
                     hoc, chord, freq, derdelta, workingdir, K_sway, K_yaw,
                     phase, feathering, Uinf, periods, savefigure, doplot):
    """
    Fits an analytical function to the given timeseries, based on oscillation
    parameters. Splits sway, yaw, acc1 and acc2 into arrays where one column
    is one oscillation, based on zero upcrossing given by upcr input
    """
    fs = FilterParam.fs
    upcr = filt.upcr
    # Fit an analytical function to sway and yaw measurements
    sway_popt = Kprofilefit(filt.time-filt.time[0], filt.sway, hoc * chord,
                            freq, 0, K_sway, 'Sway', workingdir,
                            1, doplot, savefigure)
    # Maximum yaw amplitude
    yawmax = feathering * np.arctan(hoc * chord * 2 * np.pi *
                                    freq / Uinf)
    yaw_popt = Kprofilefit(filt.time-filt.time[0], filt.yaw, yawmax,
                           freq, phase, K_yaw, 'Yaw', workingdir, 0, doplot, savefigure)

    # Initiate arrays
    xsize = int(np.max(np.round(periods * 200)))  # array size
    ysize = int(len(upcr)-1)
    time_array = createNAN(xsize, ysize)
    acc1_array = createNAN(xsize, ysize)
    acc2_array = createNAN(xsize, ysize)
    swayvel = createNAN(xsize, ysize)
    yawrate = createNAN(xsize, ysize)
    yawacc_pot = createNAN(xsize, ysize)
    yaw_array = createNAN(xsize, ysize)
    sway_array = createNAN(xsize, ysize)

    # Split time-series
    for i in range(0, len(upcr)-1):
        timetemp = filt.time[upcr[i]:upcr[i+1]] - filt.time[upcr[i]]
        time_array[0:len(timetemp), i] = timetemp
        acc1temp = filt.acc1[upcr[i]:upcr[i+1]]
        acc1_array[0:len(acc1temp), i] = acc1temp
        acc2temp = filt.acc2[upcr[i]:upcr[i+1]]
        acc2_array[0:len(acc2temp), i] = acc2temp
        # Differentiate sway and yaw to find the velocities
        swayveltemp = np.diff(filt.sway[upcr[i]:upcr[i+1]]) * fs
        # Append to assure same size as other arrays
        swayveltemp = np.append(swayveltemp,
                                (filt.sway[upcr[i+1]+1]-filt.sway[upcr[i+1]]) * fs)
        swayvel[0:len(swayveltemp), i] = swayveltemp
        yawratetemp = np.diff(filt.yaw[upcr[i]:upcr[i+1]]) * fs
        yawratetemp = np.append(yawratetemp,
                                (filt.yaw[upcr[i+1]+1]-filt.yaw[upcr[i+1]]) * fs)
        yawrate[0:len(yawratetemp), i] = yawratetemp
        # Differentiate again to find acceleration
        yawacctemp = np.diff(yawrate[:, i]) * fs
        yawacctemp = np.append(yawacctemp,
                               (yawrate[0, i] - yawrate[-1, i]) * fs)
        yawacc_pot[0:len(yawacctemp), i] = yawacctemp
        swaytemp = filt.sway[upcr[i]:upcr[i+1]]
        sway_array[0:len(swaytemp), i] = swaytemp
        yawtemp = filt.yaw[upcr[i]:upcr[i+1]]
        yaw_array[0:len(yawtemp), i] = yawtemp
        del (timetemp, acc1temp, acc2temp, yawtemp, swaytemp, swayveltemp,
             yawratetemp)

    # Calculate sway and yaw velocities based on analytical function fit
    sway_fitted = Kprofile(time_array[:, 0], *sway_popt)
    swayvel_fitted = Kprofilevelocity(time_array[:, 0], *sway_popt)
    swayacc_fitted = Kprofileacceleration(time_array[:, 0], *sway_popt)
    yaw_fitted = Kprofile(time_array[:, 0], *yaw_popt)
    yawrate_fitted = Kprofilevelocity(time_array[:, 0], *yaw_popt)
    yawacc_fitted = Kprofileacceleration(time_array[:, 0], *yaw_popt)
    # calculate yaw acceleration
    arm = 0.094
    yawacc_array = (acc1_array - acc2_array)/arm
    if doplot:  # Plot position, velocity and acceleration
        plt.figure(7,figsize=(12,8))
        plt.scatter(time_array, sway_array, color='m',
                    label='position', s=0.5)
        plt.scatter(time_array, acc1_array, color='r',
                    label='accelerometer', s=0.5)
        plt.scatter(time_array, swayvel, color='b',
                    label='velocity', s=0.5)
        plt.plot(time_array[:, 0], sway_fitted, color='k', linestyle='--',
                 linewidth=4, label="position fitted")
        plt.plot(time_array[:, 0], swayvel_fitted, color='k', linestyle='-.',
                 linewidth=4, label="velocity fitted")
        plt.plot(time_array[:, 0], swayacc_fitted, color='k', linestyle=':',
                 linewidth=4, label="acceleration fitted")
        plt.title("Fitted sway motion")
        plt.legend(loc="best")
        # Plot position, velocity and acceleration for yaw
        plt.figure(8,figsize=(12,8))

        plt.scatter(time_array, yaw_array, color='m',
                    label='position', s=0.5)

        plt.scatter(time_array, yawacc_array, color='r',
                    label='accelerometer', s=0.5)
        plt.scatter(time_array, yawacc_pot, color='c', s=0.5,
                    label='acceleration potmeter')
        plt.scatter(time_array, yawrate, color='b',
                    label='velocity', s=0.5)
        plt.plot(time_array[:, 0], yaw_fitted, color='k', linestyle='--',
                 linewidth=4, label="position fitted")
        plt.plot(time_array[:, 0], yawrate_fitted, color='k', linestyle='-.',
                 linewidth=4, label="velocity fitted")
        plt.plot(time_array[:, 0], yawacc_fitted, color='k', linestyle=':',
                 linewidth=4, label="acceleration fitted")
        plt.title("Fitted yaw motion")
        plt.legend(loc="best")
        # Save figures
        if savefigure is 1:
            plt.savefig(workingdir + 'plots/' + 'yawmotion.pdf')
            plt.figure(7)
            plt.savefig(workingdir + 'plots/' + 'swaymotion.pdf')
    return (time_array, sway_popt, swayvel, acc1_array,
            yaw_popt, yawrate, yawacc_array)


def splittimeseries(raw, Nskip, derdelta, workingdir, yawsplit,
                    savefigure, doplot):
    """
    Split the timeseries and perform averaging between oscillations, return
    sway and yaw positions, velocities and accelerations. Set yawsplit to 1
    if yaw measurement should be used to determine oscillation periods
    """
    datatuple = namedtuple('datatuple', 'time sway yaw mz acc1 acc2 fx fy upcr')
    time = raw.time
    sway = raw.sway
    yaw = raw.yaw
    errormessage = 0
    # Determine if sway or yaw is used to determine oscillation period
    if yawsplit == 1:
        pos = yaw
        N = 1
    elif yawsplit == 0:
        pos = sway
        N = 2
    # Initiate variable
    upcr = np.empty([1, 0], int)
    # Check where the sway position crosses the zero position
    # and has a positive derivative
    for i in range(0, len(pos)-1):
        if pos[i+1] > 0 and (
                pos[i] < 0 and
                FilterParam.fs * (pos[i+1] - pos[i]) > derdelta):
            upcr = np.append(upcr, i)
            if doplot:
                plt.figure(3)
                plt.subplot(2, 1, N)
                plt.vlines(time[i], max(pos), min(pos))
            if abs(sway[i]) > tol.swaycross:
                print('Recorded zero upcrossing of sway'
                      'position is above %2.0f mm!' % tol.swaycross)
                errormessage = 2
    if doplot:  # Plot timeseries of sway and yaw, and denote split lines
        plt.figure(3)
        plt.subplot(2, 1, 1)  # Yaw position
        plt.title("position measurements")
        plt.ylabel('angle [rad]')
        plt.plot(time, yaw, label='yaw')
        plt.subplot(2, 1, 2)
        plt.ylabel('pos [m]')
        plt.plot(time, sway, label='sway')
        for i in [1, 2]:
            plt.subplot(2, 1, i)
            plt.grid(True)
            plt.axis('tight')
            plt.legend(loc='best')
            plt.xlabel('time [s]')
        if savefigure is 1:
            plt.savefig(workingdir + 'plots/' + 'splitlines.pdf')
    # Check if the recorded oscillation periods are within a given error
    periods = np.empty([len(upcr) - 2 * Nskip+1])
    for i in range(Nskip, len(upcr) - Nskip+1):
        periods[i-Nskip] = time[upcr[i+1]]-time[upcr[i]]
    meanperiod = np.mean(periods)
    for i in range(0, len(periods)):
        temp = (meanperiod - periods[i]) / meanperiod
        if temp > tol.period:
            print('The error in oscillation period relative to mean'
                            'is outside the set tolerance, the maximum error'
                            ' is %2.1f' % temp)
            errormessage = 2
    """
    # Split the rawdata
    cut = datatuple(time=raw.time[upcr[0]:upcr[-1]],
                    sway=raw.sway[upcr[0]:upcr[-1]],
                    yaw=raw.yaw[upcr[0]:upcr[-1]],
                    mz=raw.mz[upcr[0]:upcr[-1]],
                    acc1=raw.acc1[upcr[0]:upcr[-1]],
                    acc2=raw.acc2[upcr[0]:upcr[-1]],
                    fx=raw.fx[upcr[0]:upcr[-1]],
                    fy=raw.fy[upcr[0]:upcr[-1]],
                    upcr=upcr-upcr[0])
    """
    return (periods, upcr, errormessage)


def cutseries(filt, datatuple, upcr, Nskip):
    cut = datatuple(time=filt.time[upcr[Nskip]:upcr[-Nskip]+2],
                    sway=filt.sway[upcr[Nskip]:upcr[-Nskip]+2],
                    yaw=filt.yaw[upcr[Nskip]:upcr[-Nskip]+2],
                    mz=filt.mz[upcr[Nskip]:upcr[-Nskip]+2],
                    acc1=filt.acc1[upcr[Nskip]:upcr[-Nskip]+2],
                    acc2=filt.acc2[upcr[Nskip]:upcr[-Nskip]+2],
                    fx=filt.fx[upcr[Nskip]:upcr[-Nskip]+2],
                    fy=filt.fy[upcr[Nskip]:upcr[-Nskip]+2],
                    upcr=upcr[Nskip:1-Nskip]-upcr[Nskip])
    return cut


def plotefficiency(ct, efficiency, ctmin, statuscode, color='b'):
    """
    Plot the thrust and efficiency datapoint, used to display the progress of
    the oscillating foil optimisation. If init is true the plot window is
    initialised
    """
    # Check if the value is rejected or not due to low thrust
    if ct > ctmin:
        m = color
    else:
        m = 'r'

    # Check which subplot to select
    if statuscode is 0:
        subnr = int(1)
    elif statuscode is 1:
        subnr = int(2)
    elif statuscode is 2:
        subnr = int(3)
    else:
        subnr = int(3)

    fig = plt.figure(50)
    # fig.show()
    ax = plt.subplot(3, 1, subnr)
    plt.scatter(ct, 100 * efficiency, color=m, marker='x')
    plt.ylim((0, 100))
    (xmin, xmax) = plt.gca().get_xlim()
    if ct > xmax:
        for i in [1, 2, 3]:
            plt.subplot(3, 1, i)
            plt.xlim((ctmin-0.03, ct+0.01))
            #plt.xlim((ctmin-0.03, 0.6))


def plotcsvlog_efficiency(ctmin, csvfilename, output_file):
    """
    Read input csv file, and plot the results as ct vs efficiency
    """
    import csv
    # Clear old data in figure
    fig = plt.figure(50, figsize=(16, 12))
    fig.clear

    # Initiate figure and grid
    for subnr in [1, 2, 3]:
        ax = plt.subplot(3, 1, subnr)

        plt.ylabel('Efficiency [%]')
        if subnr is 1: plt.title('Accepted individuals')
        elif subnr is 2: plt.title('Retried individuals')
        elif subnr is 3:
            plt.title('Rejected individuals')
            plt.xlabel('Thrust coefficient [-]')
        plt.vlines(ctmin, 0, 100, color='r')

        # set up grid
        major_ticks = np.arange(0, 101, 20)
        minor_ticks = np.arange(0, 101, 5)
        ax.set_yticks(major_ticks)
        ax.set_yticks(minor_ticks, minor=True)
        plt.grid(which='both')
        ax.grid(which='minor', alpha=1)
        ax.grid(which='major', alpha=0.5)
        # fig.show()

        # Plot ideal efficiency
        x = np.linspace(0, 1.5, 50)
        plt.plot(x, 100 * idealefficiency(x), color='b')
        plt.xlim((ctmin-0.03, 0.1))

    # Read CSV file and plot the results
    with open(os.path.join(csvfilename)) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            statuscode = int(row['Statuscode'])
            ct = float(row['CT'])
            efficiency = float(row['efficiency'])
            plotefficiency(ct, efficiency, ctmin, statuscode, color='g')
    # Save figure
    plt.savefig(output_file)


def updateerrormessage(old, new):
    """
    Updates the errormessage if the new value is larger than the old one
    """
    if new > old:
        output = new
    else:
        output = old
    return output

def process_results(filename, Nskip, derdelta,
                    workingdir, correct_drag, savefigure=0, doplot=True, yawsplit=0, latexplot=False):
    """
    Performs all necessary file processing to create a filtered signal, which
    is cut to a given number of oscillations
    """
    # Read Raw data
    raw, datatuple = readrawdata(filename)

    # Perform lowpassfiltering and drift correction
    filt, drift = filterdata(raw, correct_drag, workingdir, doplot, savefigure, latexplot=latexplot)

    # Plot timeseries of the filtered acc measurement
    if doplot:
        plt.figure(4)
        plt.subplot(2, 1, 1)
        plt.plot(raw.time, raw.acc1, ls='--')
        plt.plot(filt.time, filt.acc1, ls='-')
        plt.subplot(2, 1, 2)
        plt.plot(raw.time, raw.acc2, ls='--')
        plt.plot(filt.time, filt.acc2, ls='-')


    # Split the timeseries
    periods, upcr, newerror = splittimeseries(filt, Nskip,
                                                    derdelta,
                                                    workingdir, yawsplit,
                                                    savefigure, doplot)

    # Cut Nskip from the beginning and end of the series
    cut = cutseries(filt, datatuple, upcr, Nskip)
    upcr = cut.upcr
    return cut, periods, drift, newerror

def read_runscsv(runsfile, findAoA=True):
    """
    Reads the CSV file from the oscillating foil optimisation experiment, and
    returns efficiency, CT, and fitted motion parameters. Excludes retried
    individuals
    """
    import csv
    # Read CSV file and plot the results
    absnr = []
    efficiency = []
    ct = []
    st = []
    sway_amp = []
    phase = []
    feathering = []
    k_sway = []
    k_yaw = []
    fx_drift = []
    fy_drift = []
    mz_drift = []
    statuscode = []
    AoA = []
    gen = []
    timestamp = []
    with open(os.path.join(runsfile)) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            # Errorcode
            status = int(row['Statuscode'])
            cttemp = float(row['CT'])
            if status == 0 and cttemp > 0:
                statuscode.append(status)
                absnr.append(float(row['Absolute_number']))
                efficiency.append(float(row['efficiency']))
                ct.append(cttemp)
                # Motion parameters
                st.append(float(row['St_fit']))
                sway_amp.append(float(row['hoc_fit']))
                phase.append(float(row['phase_fit']))
                feathering.append(float(row['feathering']))
                k_sway.append(float(row['K_sway_fit']))
                k_yaw.append(float(row['K_yaw_fit']))
                # Sensor drift
                fx_drift.append(float(row['fx drift']))
                fy_drift.append(float(row['fy drift']))
                mz_drift.append(float(row['mz drift']))
                if findAoA:
                    AoA.append(0.5*(abs(float(row['max_AoA'])) + abs(float(row['min_AoA']))))
                gen.append(float(row['Generation']))
                try:
                    timestamp.append(row['timestamp'])
                except:
                    timestamp.append(0)
    return (ct, efficiency, statuscode, st,
            sway_amp, phase, feathering, k_sway, k_yaw,
            fx_drift, fy_drift, mz_drift, AoA, gen, timestamp, absnr)


def findfoldername(absnr, csvfilename):
    import csv
    with open(csvfilename) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        for row in reader:
            if float(row['Absolute_number']) == absnr:
                foldername = row['runname']
                case = row['Foldername']

    return foldername, case







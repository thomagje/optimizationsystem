#!/usr/bin/python3
"""
Genetic algorithm for optimizing movement pattern
of a biomimetic hydrofoil
@author: Thomas
"""
import random
import json
import time
import os
import threading
import queue

from copy import deepcopy
from cwt_runner import CWT
from ga_graphics import GAGraphics
from ga_gui import init_gui


class GeneticAlgorithm(object):
    """
    Genetic algorithm controller
    Params: MovementOptimization object
    """
    def __init__(self, problem, fitness_goal):
        """
        Init GeneticAlgorithm
        Params:
        MovementOptimization object
        Fitness goal
        """
        self.problem = problem
        self.fitness_goal = fitness_goal
        self.decimate = False

    def run(self, population=None):
        """
        Run algorithm
        Generates initial population and calls evolve() until completed
        Returns phenotype of best individual
        """
        best = None
        if population is None:
            population = self.problem.initial_population()
        while self.problem.generation_count < \
                self.problem.generations:
            population_fitness = self.calc_fitness(population)
            self.problem.save_population(population, population_fitness)
            print("------------------------------------")
            print("Generation {} completed".format(
                self.problem.generation_count))
            print("Best individual:")
            if best is not None:
                best = max(max(population_fitness), best)
            else:
                best = max(population_fitness)
            print("Eff=" + str(best[0]))
            print(self.problem.phenotype(best[1]))
            print("------------------------------------")
            if best[0] >= self.fitness_goal:
                print("Goal reached. Ending run...")
                return best
            time.sleep(5)
            population = self.evolve(population_fitness)
        print("Complete")
        self.problem.save_population(population)
        return sorted(self.calc_fitness(population), reverse=True)[0]

    def calc_fitness(self, population):
        """
        Calculate fitness of entire population
        """
        population_fitness = []
        for individual in population:
            new_ind = deepcopy(individual)
            self.problem.graphics.set_generation(
                self.problem.generation_count)
            self.problem.graphics.draw_population(population_fitness)
            phenotype = self.problem.phenotype(new_ind)
            f_res = self.problem.fitness(new_ind)
            fit1, fit2, status = f_res
            strength = 0.2
            while status is not 0:
                print("Garbage individual detected")
                self.problem.num_discarded += 1
                self.problem.graphics.set_generation(
                    self.problem.generation_count)
                self.problem.graphics.draw_population(
                    population_fitness)
                if strength > 1.0:
                    print("Generating replacement...")
                    new_ind = self.problem.generate_random_individual()
                else:
                    print("Mutating with {}% and retrying..."
                          .format(strength*100))
                    self.problem.mutate(new_ind)
                    strength += 0.2
                phenotype = self.problem.phenotype(new_ind)
                f_res = self.problem.fitness(new_ind, True)
                fit1, fit2, status = f_res
            population_fitness.append((fit1, new_ind, phenotype, fit2))
            print("")
        print("")
        return population_fitness

    def evolve(self, population_fitness):
        """
        Run a generation of the genetic algorithm and return next generation
        Params: List of individuals with fitness
        """
        self.problem.generation_count += 1
        parents = self.problem.parent_pairs(population_fitness)
        next_generation = []
        for parent_pair in parents:
            if random.random() < self.problem.crossover_rate:
                individuals = self.problem.crossover(parent_pair[0],
                                                     parent_pair[1])
            else:
                individuals = parent_pair
            for individual in individuals:
                if random.random() < self.problem.mutation_rate:
                    self.problem.mutate(individual)
                next_generation.append(deepcopy(individual))
        if self.decimate:
            print("Decimating population")
            self.decimate = False
            random.shuffle(next_generation)
            next_generation = next_generation[:int(len(next_generation)/2)]
            while len(next_generation) < self.problem.population_size:
                next_generation.append(
                    self.problem.generate_random_individual())
            print("Half of population replaced")

        next_generation.append(deepcopy(max(population_fitness)[1]))  # elitism
        return next_generation

    def resume_run(self, generation_num):
        """
        Resume an existing run from disk
        """
        generation_folder = os.path.join(self.problem.run_dir,
                                         'population',
                                         'generation_' + str(generation_num))
        pop_file = os.path.join(generation_folder, 'population.json')
        run_file = os.path.join(generation_folder, 'runs.json')
        generation = None
        with open(pop_file, "r") as json_file:
            generation = json.load(json_file)
        calculated_fitness = None
        with open(run_file, "r") as json_file:
            calculated_fitness = json.load(json_file)
        self.problem.calculated_fitness = calculated_fitness
        self.problem.generation_count = generation_num
        return self.run(population=generation)


class MovementOptimization(object):
    """
    Class containing all genetic methods
    for the foil movement optimization problem
    """
    def __init__(self,
                 run_dir,
                 sensor_dir,
                 backup_dir,
                 rio_ip,
                 variables,
                 Uinf,
                 num_periods,
                 minimum_thrust,
                 callback_queue,
                 correct_drag=0,
                 maximum_thrust=None,
                 generations=10,
                 population_size=10,
                 mutation_rate=0.7,
                 mutation_strength=0.05,
                 crossover_rate=0.7,
                 tournament_size=4):
        """
        Init MovementOptimization object
        Params:
        Working directory
        Sensor file directory
        Backup file storage directory
        Ip of cRio interface
        Variable config list
        CWT speed
        Number of oscillations per run
        Minimum thrust limit
        GUI callback queue
        Measured drag
        Maximum thrust limit
        Number of generations
        Number of individuals in population
        Rate of mutation (0.0-1.0)
        Rate of crossover (0.0-1.0)
        Number of individuals in tournament (< population_size)

        """
        self.generations = generations
        self.population_size = population_size
        self.mutation_rate = mutation_rate
        self.crossover_rate = crossover_rate
        self.tournament_size = tournament_size
        self.calculated_fitness = {}
        self.generation_count = 0
        self.variables = variables  # List of [Minval, Maxval, num_bits, name]
        self.cwt = CWT(run_dir, sensor_dir,
                       backup_dir, rio_ip, [x[3] for x in variables],
                       Uinf, num_periods, correct_drag)
        self.graphics = GAGraphics(callback_queue)
        self.minimum_thrust = minimum_thrust
        self.maximum_thrust = maximum_thrust
        self.run_dir = run_dir
        self.mutation_strength = mutation_strength
        self.num_discarded = 0
        if not os.path.exists(run_dir):
            os.makedirs(run_dir)

    def mutate(self, genotype, strength=None):
        """
        Make a random change to the genotype
        """
        strength = self.mutation_strength if strength is None else strength
        total_bits = sum([v[2] for v in self.variables])
        num_changes = max(round(total_bits*strength), 1)
        for _ in range(0, num_changes):
            variable_index = random.randint(0, len(genotype) - 1)
            bit_index = random.randint(0, len(genotype[variable_index]) - 1)
            genotype[variable_index][bit_index] ^= 1

    def crossover(self, parent1, parent2):
        """
        Cross two genotypes
        """
        geno_length = 0
        for variable in parent1:
            geno_length += len(variable)
        index = random.randint(1, geno_length - 2)

        child1 = parent1[:]
        child1_count = 0
        for idx, variable in enumerate(child1):
            for bit_index in range(0, len(variable)):
                if child1_count >= index:
                    variable[bit_index] = parent2[idx][bit_index]
                child1_count += 1

        child2 = parent2[:]
        child2_count = 0
        for idx, variable in enumerate(child2):
            for bit_index in range(0, len(variable)):
                if child2_count >= index:
                    variable[bit_index] = parent1[idx][bit_index]
                child2_count += 1

        return (child1, child2)

    def phenotype(self, genotype):
        """
        Generate usable data from genotype
        """
        pheno = []
        for idx, bit_group in enumerate(genotype):
            variable = 0
            for bit in bit_group:
                if bit:
                    variable += (self.variables[idx][1] -
                                 self.variables[idx][0])/self.variables[idx][2]
            variable += self.variables[idx][0]
            pheno.append(variable)
        return pheno

    def fitness(self, genotype, rerun=False):
        """
        Calculate fitness of genotype
        """
        phenotype = self.phenotype(genotype)
        phenotype_str = self.serialize_phenotype(phenotype)

        if phenotype_str in self.calculated_fitness.keys():
            print("Fetching fitness from memory...")
            return self.calculated_fitness[phenotype_str][0]

        self.graphics.set_info("Retries: {}\n"
                               "Discarded: {}".format(self.cwt.num_retries,
                                                      self.num_discarded))
        self.graphics.set_current_run(
            "\n".join(self.phenotype_human_readable(phenotype)), rerun)

        fitness = self.run_cwt(phenotype)
        self.calculated_fitness[phenotype_str] = (fitness,
                                                  self.generation_count)
        return fitness

    def run_cwt(self, phenotype):
        """
        Do a run in the tank and return sensor data
        Params: Array of attributes
        """
        result = self.cwt.run(phenotype, self.generation_count)
        if result[1] < self.minimum_thrust:
            print("Thrust too low {}".format(result[1]))
            return (0.0, result[1], result[2])
        elif self.maximum_thrust is not None and \
                result[1] > self.maximum_thrust:
            print("Thrust too high {}".format(result[1]))
            return (0.0, result[1], result[2])
        else:
            return result

    def one_max_fitness(self, phenotype):
        """
        Test fitness function
        """
        fitness = 0.0
        for variable in phenotype:
            if variable == 1.0:
                fitness += 1.0/float(len(phenotype))
        return (fitness, 0.0, 0)

    def initial_population(self):
        """
        Generate initial population
        """
        population = []
        for _ in range(0, self.population_size):
            population.append(self.generate_random_individual())
        return population

    def parent_pairs(self, population_fitness):
        """
        Run parent selection and return a pair
        """
        pairs = []
        for _ in range(0, int(self.population_size/2)):
            parent1 = self.tournament(population_fitness)
            parent2 = self.tournament(population_fitness)
            pairs.append((deepcopy(parent1), deepcopy(parent2)))
        return pairs

    def tournament(self, population_fitness):
        """
        Run a tournament with random individuals and return winner
        """
        individuals = []
        for _ in range(0, self.tournament_size):
            individuals.append(self.random_individual(population_fitness))
        sort_int = sorted(individuals, reverse=True)
        return sort_int[0][1]

    @staticmethod
    def random_individual(population):
        """
        Get random individual from population
        """
        return population[random.randint(0, len(population) - 1)]

    def save_population(self, population, calc_population):
        """
        Save entire population to file
        """
        folder = os.path.join(self.run_dir,
                              "population",
                              "generation_" + str(self.generation_count))
        pop_file = os.path.join(folder, "population.json")
        calc_pop_file = os.path.join(folder, "population_w_fitness.json")
        run_file = os.path.join(folder, "runs.json")
        if not os.path.exists(folder):
            os.makedirs(folder)
        with open(pop_file, "w") as json_file:
            json.dump(population, json_file)
        with open(run_file, "w") as json_file:
            json.dump(self.calculated_fitness, json_file)
        with open(calc_pop_file, "w") as json_file:
            json.dump(calc_population, json_file)

    @staticmethod
    def serialize_phenotype(phenotype):
        """
        Convert phenotype to string
        """
        return ','.join("{0:g}".format(x) for x in phenotype)

    @staticmethod
    def serialize_genotype(genotype):
        """
        Convert genotype to string
        """
        return json.dumps(genotype)

    @staticmethod
    def phenotype_human_readable(phenotype):
        """
        Convert phenotype to human readable format
        """
        return ["k_yaw: " + "%.2f" % phenotype[0],
                "strouhals: " + "%.2f" % phenotype[1],
                "k_sway: " + "%.2f" % phenotype[2],
                "phase_diff: " + "%.2f" % phenotype[3],
                "feathering: " + "%.2f" % phenotype[4],
                "sway_amp: " + "%.2f" % phenotype[5]]

    def generate_random_individual(self):
        """
        Generate a random individual
        """
        genotype = []
        for variable in self.variables:
            ones = random.randint(0, variable[2])
            bit_group = [1]*ones + [0]*(variable[2]-ones)
            random.shuffle(bit_group)
            genotype.append(bit_group)
        return genotype


def main():
    """
    Main function
    """
    callback_queue = queue.Queue()
    var_list = []
    var_list.append([-0.8, 2.0, 27, 'k_yaw'])  # 0
    var_list.append([0.05, 0.18, 25, 'strouhals_num'])  # 1
    var_list.append([-0.8, 2.0, 27, 'k_sway'])  # 2
    var_list.append([3.14/4, 3*3.14/4, 80, 'phase_difference'])  # 3
    var_list.append([0.0, 1.0, 49, 'feathering'])  # 4
    var_list.append([1.0, 1.2, 2, 'sway_amp'])  # 5
    base_dir = "test_folder"
    run_name = "test_run"
    run_dir = os.path.join(base_dir, run_name)
    if not os.path.exists(run_dir):
        os.makedirs(run_dir)
    mo = MovementOptimization(
        run_dir,
        os.path.join(base_dir, "AquireMeasurement"),
        os.path.join(base_dir, "PreviousMeasurements", run_name),
        '192.168.1.7',
        var_list,
        0.5242,
        20,
        0.0,
        callback_queue,
        correct_drag=0.180746921,
        population_size=20,
        mutation_rate=0.2,
        crossover_rate=0.7,
        mutation_strength=0.05,
        tournament_size=4,
        generations=1000,
        maximum_thrust=0.2)
    ga = GeneticAlgorithm(mo, 1.0)
    t = threading.Thread(target=ga.run)
    t.daemon = True
    t.start()
    init_gui(callback_queue, ga)
    print("Completed")

if __name__ == "__main__":
    main()

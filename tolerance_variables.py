# -*- coding: utf-8 -*-
"""
Created on Fri Mar  3 12:53:01 2017
Contains global variables for tolerance functions
@author: Lennard
"""


class tol:
    swaycross = 0.005  # meters
    period = 0.05  # Relative error of one period compared to mean
    thrust = 0.05  # Thrust precision error between oscillations
    power = 0.05  # Power consumption precsion error between oscillations
    strouhal = 0.005  # Deviation in strouhal number
    K_sway = 5  # Deviation in K_sway parameter
    K_yaw = 5  # Deviation in K_yaw parameter
    feathering = 0.2  # Deviation in feathering parameter
    phase = 15 * 3.14 / 180  # Deviation in phase
    swayamp = 0.3  # Deviation in sway amplitude (non-dimensionalised)
    """
    period = 999
    thrust = 999
    power = 999
    strouhal = 999
    K_sway = 999
    K_yaw = 999
    feathering = 999
    phase = 999
    swayamp = 999
    """